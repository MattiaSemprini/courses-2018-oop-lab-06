package it.unibo.oop.lab.collections1;

import java.util.Set;
import java.util.TreeSet;

/**
 * Example performance measuring. Use this class as working example of how to
 * measure the time necessary to perform operations on data structures.
 */
public class TestPerformance {

    private static final int ELEMS = 1000000;
    private static final int TO_MS = 1000000;
   long startime;  
    
    
    public void start(){
        startime = System.nanoTime();
        }
        			//return ms time after start function
        public long stop(){
        startime = System.nanoTime() - startime;
       startime = startime/ TO_MS;
        return startime;
        }
    /**
     * @param s
     *            ignored
     */
//    public static void m(final String... s) {
//        /*
//         * Set up the data structures
//         */
//        final Set<String> set = new TreeSet<>();
//        /*
//         * Prepare a variable for measuring time
//         */
//        long time = System.nanoTime();
//        /*
//         * Run the benchmark
//         */
//        for (int i = 1; i <= ELEMS; i++) {
//            set.add(Integer.toString(i));
//        }
//        /*
//         * Compute the time and print result
//         */
//        time = System.nanoTime() - time;
//        System.out.println("Converting " + ELEMS
//                + " int to String and inserting them in a Set took " + time
//                + "ns (" + time / TO_MS + "ms)");
//        //System.out.println(set);
//    }

}
