package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	ArrayList<Integer> numbers = new ArrayList<Integer>();
    	numbers.ensureCapacity(1001);
    	for (int i = 1000; i < 2000; i++) {
			numbers.add(i);
			
		}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	LinkedList<Integer> numberlinked = new LinkedList<Integer>();
    	numberlinked.addAll(numbers);
    	/*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	Integer i = numbers.get(0);
    	numbers.set(0,numbers.get(numbers.size()-1) );
    	numbers.set(numbers.size()-1 , i);
    	/*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer integer : numbers) {
			System.out.println(integer.toString());
		}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	TestPerformance firstTest = new TestPerformance();
    	TestPerformance secondTest = new TestPerformance();
    	
    	firstTest.start();
    	for (int j = 0; j < 100000; j++) {
    		numbers.add(j, j);
    	}
    	System.out.println("Array list ha impiegato " + firstTest.stop());
    
    
    	firstTest.start();
    	for (int j = 0; j < 100000; j++) {
    		numberlinked.addFirst(j);
    	}
    	System.out.println("Linked list ha impiegato " + firstTest.stop());
    	
    	/*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	Random r = new Random(System.nanoTime());
    	Collection<Integer> io = new ArrayList<Integer>();
    	firstTest.start();
    	for (int j = 0; j < 1000; j++) {
    		io.add(numbers.get(r.nextInt(1000)));
    	}
    	firstTest.stop();
    
    
    	secondTest.start();
    	for (int j = 0; j < 1000; j++) {
    		io.add(numberlinked.get(r.nextInt(1000)));
    	}
    	secondTest.stop();
    	for (Iterator iterator = io.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			
			
		}
    	System.out.println("Array list ha impiegato a leggere " + firstTest.startime);
    	System.out.println("Linked list ha impiegato a leggere " + secondTest.startime);
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    }
}
